import {createContext} from 'react';

export const temi = {
    light: {
        color: "dodgerblue",
        background: "#eeeeee"
    },
    dark: {
        color: "#ffffff",
        background: "#222222"
    }
};

export const ThemeContext = createContext(temi);
