import React, {useState, useEffect} from 'react';
import './App.css';
import GenericComponent from "./components/GenericComponent/GenericComponent";
import {ThemeContext, temi} from "./context/tema";
import Toolbar from "./components/Toolbar/Toolbar";
import CounterUseReducer from "./components/CounterUseReducer/CounterUseReducer";
import CounterUseCallback from "./components/CounterUseCallback/CounterUseCallback";

function App() {
    const [showComponent, setShowComponent] = useState(true);
    const [tema, setTema] = useState('dark');

    /**
     * useEffect è una funziona che lavora come la componentDidMount, componentDidUpdate e la componentWillUnmount
     * Accetta in ingresso 2 paremetri: una funzione e un array di dipendenze
     * La useEffect eseguirà il suo contenuto ogni volta che almeno una delle sue dipendenze cambia (comportamento componentDidUpdate).
     * Se vogliamo che si comporti come la componentDidMount l'array delle dipendenze deve essere vuoto.
     * Per farla lavorare come componentWillUnmount dobbiamo inserire al suo interno una return con una funzione.
     * Quest'ultima funzione è la componentWillUnmount
     */
    useEffect(() => {
        setTimeout(() => {
            setShowComponent(false);
        }, 4000);

        return () => { //return con una funzione => componentWillUnmount.
            //... contenuto della componentWillUnmount
        }
    }, []); //Array delle dipendenze vuote => componentDidMount

    function toggleTema() {
        setTema(tema === 'dark' ? 'light' : 'dark');
    }

    return (
        <div className="App">
            {showComponent && <GenericComponent />}
            <button onClick={toggleTema}>cambia tema</button>
            <ThemeContext.Provider value={temi[tema]}>
                <Toolbar />
            </ThemeContext.Provider>
            <CounterUseReducer/>
            <CounterUseCallback/>
        </div>
    );
}

export default App;
