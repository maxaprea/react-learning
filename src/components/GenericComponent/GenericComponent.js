import React, {useState, useEffect} from "react";

export default function GenericComponent() {
    const [counter, setCounter] = useState(null);

    useEffect(() => {
        setCounter(0);
    }, []);

    useEffect(() => {
        console.log(`counter: ${counter} - ${new Date().getTime()}`);

        return () => {
            console.log('clean')
        }
    });

    return (
        <div className="App">
            <button onClick={() => setCounter(counter+1)}>click</button>
            <p>{counter}</p>
        </div>
    );
};
