import React, {useReducer} from "react";

const initialState = {counter: 0};

const reducer = (state, action) => {
    switch (action.type) {
        case 'incrementa': {
            return {counter: state.counter + 1};
        }
        case 'decrementa': {
            return {counter: state.counter - 1};
        }
        case 'reset': {
            return init(); //posso anche restituire direttamente initialState senza usare init
        }
        default:
            return {...state};
    }
};

const init = () => {
    return initialState;
};

export default function CounterUseReducer() {
    const [state, dispatch] = useReducer(reducer, initialState);
    //const [{counter}, dispatch] = useReducer(reducer, initialState, init);

    return (
        <>
            Contatore: {state.counter}
            <button onClick={() => dispatch({type: 'incrementa'})}>incrementa contatore</button>
            <button onClick={() => dispatch({type: 'decrementa'})}>decrementa contatore</button>
            <button onClick={() => dispatch({type: 'reset'})}>reset</button>
        </>
    );
}
