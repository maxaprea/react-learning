import React, {useCallback, useState, useMemo} from "react";

const ff = new Set();

export default function CounterUseCallback() {
    const [counter, setCounter] = useState(0);

    //useCallback mette in cache la definizione di una funzione ed evita che questa venga ricreata a ogni render.
    //In questo modo la funzione viene ricreata solo quando almeno una delle dipendenze cambia.
    //Ogni funzione dichiarata all'interno di un componente funzione dovrebbe essere memoizzata con useCallback.
    //Se usa funzioni o altre variabili dallo scope del componente, dovrebbe elencarle tra le sue dipendenze.
    const incrementa = () => setCounter(counter + 1);
    const incrementaMemoized = useCallback(() => setCounter(counter + 2), [counter]);

    //useMemo restituisce mette in cache il valore dato dalla funzione (factory) che gli viene passata
    //E' utile quando il l'esecuzione della funzione richiede molte risorse e non la si vuole ripete a ogni render
    // ma solo quando variano uno o più valori (le dipendenze)
    const inc = useMemo(() => counter + 2, [counter]);

    ff.add(incrementa);
    //incrementaMemoized viene ricreata solo quando cambia counter.
    ff.add(incrementaMemoized);
    console.log(ff);

    return (
        <div>
            useCallback Counter: {counter}
            useCallback inc: {inc}
            <button onClick={incrementa}>incrementa</button>
        </div>
    );
}
