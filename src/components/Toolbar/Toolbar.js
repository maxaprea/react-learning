import React, {useContext} from "react";
import {ThemeContext} from '../../context/tema';

export default function Toolbar() {
    /**
     * useContext(MyContext) consente solo di leggere il context e intercettare le sue modifiche.
     * Per rendere leggibile il context in questo componente bisogna inserire <MyContext.Provider> in uno dei componenti antenati, in questo caso App.js
     * Questo componente intercetterà il <ThemeContext.Provider> del suo antenato più vicino
     * @type {{light: {color: string, background: string}, dark: {color: string, background: string}}}
     */
    const tema = useContext(ThemeContext);

    return (
        <div style={{ background: tema.background, color: tema.color }}>tema</div>
    );
}
